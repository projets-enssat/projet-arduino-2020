#ifndef ultrason
#define ultrason

#include "Arduino.h"

// constantes globale
const unsigned long MEASURE_TIMEOUT = 25000UL; // 25ms = ~8m à 340m/s
const float SOUND_SPEED = 340.0 / 1000; // mm/µs

class Ultrason{
  public:
    Ultrason(byte trigger, byte echo);
    void init();
    float getDistance_mm();
    float getDistance_cm();
    float getDistance_m();
  private:
    // broches
    byte _trigger;
    byte _echo;
};

#endif