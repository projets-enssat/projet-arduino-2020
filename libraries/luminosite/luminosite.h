#ifndef lumi
#define lumi

#include "Arduino.h"

const float LUMINOSITE_JOUR = 0.3;

class Luminosite{
  public:
    // constructeur
    Luminosite(int pin);
    void init();
    float getLuminosite();
  private:
    int lightPin;
};

#endif
