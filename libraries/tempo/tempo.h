#ifndef tempo
#define tempo

// PLUS PETITE FRACTION TEMPORELLE
const int dt = 100;

// RAPPORT ENTRE 1 SEC ET dt, (s * dt = 1000)
const float s = 1000 / dt;

#endif
