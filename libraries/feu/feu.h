#ifndef fire
#define fire

#include "Arduino.h"
#include "tempo.h"

// VALEURS POSSIBLES POUR LE MODE DE FONCTIONNEMENT DU FEU
const int NORMAL = 0;
const int NUIT = 1;
const int WARNING = 2;


// DUREE DES COULEURS POUR LE MODE NORMAL
const int TV = 5000; // TV = duree vert en ms
const int TO = 2000; // TO = duree orange en ms
const int TR = TV + TO; // TR = duree rouge en ms
const int TTOTAL = TV + TO + TR; // TTOTAL = duree rouge en ms

// DUREE DE LA COULEUR ORANGE POUR LE MODE WARNING
const int TW = 1000;


// VALEUR POSSIBLE POUR LES COULEURS DU FEU
const int ROUGE = 0;
const int ORANGE = 1;
const int VERT = 2;
const int AUCUNE = 3;


// DEFINITION DE LA CLASSE FEU
class Feu{
  public:
    // constructeur
    Feu(int LV, int LO, int LR);
    void init();
    int getCouleur();
    void setCouleur(int couleur);
    int getMode();
    void setMode(int mode);
    void updateLuminosite(float luminosite);
  private:
    int _couleur;
    float _luminosite;
    int _LV;
    int _LO;
    int _LR;
    int _mode; // au choix parmi [NORMAL, NUIT, WARNING]
};

#endif
