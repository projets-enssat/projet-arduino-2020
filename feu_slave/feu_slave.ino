/* include libraries */
  #include <feu.h>
  #include <ultrason.h>

/* pin define */
 /** Bluetooth **/
  const byte Key = 7;

  /** Feux Tricolore **/
  const int LV = 11;
  const int LO = 12;
  const int LR = 13;
  const byte buttonPin = 8;

  /** Ultrason **/ 
  const byte triggerPin = 2;
  const byte echoPin = 3;

/* Constante define */
  Feu feu(LV, LO, LR);
  Ultrason ultra(triggerPin, echoPin);
  int warningCount = 0;
  
void setup() {
  feu.init();
  ultra.init();

  pinMode(buttonPin, INPUT);
  pinMode(Key, OUTPUT);
  digitalWrite(Key, LOW);
  
  Serial.begin(9600);

  delay(1000);
  digitalWrite(Key, HIGH);

/** Initialiser le module HC-05 en esclave**/
  
  Serial.println("AT+NAME=HC05-slave");
  Serial.println("AT+PSWD=1234");
  Serial.println("AT+ROLE=0");
  Serial.println("AT+CMODE=0");
  Serial.println("AT+RMAAD"); // Supprime les anciennes conenxions misent en cache
  
  // mode warning tant que l'arduino maître n'a pas débuté son programme
  feu.setMode(WARNING);
  digitalWrite(Key, LOW);
}

void loop() {
  // Reception du message pour changements internes
  
  String msg = "";
  if(Serial.available()){
    msg = Serial.readString(); 
  }
  
  String key="";
  String value="";

  // fonction pour récupérer la valeur donné après ":"
  for(int i=0;i<msg.length();i++){
    if(msg[i] == ':'){
      key=msg.substring(0,i);
      value=msg.substring(i+1,i+2);
      warningCount=0;
      feu.setMode(NORMAL);
      break;
    }
  }
  if(key.equals("MODE")){
    feu.setMode(value.toInt());
  } else if(key.equals("COLOR")){
    feu.setCouleur(value.toInt());
  }
  else {
    // si il n'y a pas eu de réponse depuis x temps, mode warning
    if(warningCount > 80){
      feu.setMode(WARNING);
    }
    delay(100);
    warningCount++;
  }

  if(feu.getMode() == WARNING){
      // mode WARNING
      feu.setCouleur(ORANGE);
      feu.updateLuminosite(0.0);
      delay(200);
      feu.setCouleur(AUCUNE);
  }

  feu.updateLuminosite(0.0);

  // Emission des données

  int statusPieton = digitalRead(buttonPin);
  if(statusPieton == HIGH){
    Serial.println("PIETON");
  }
  
  float voiture = ultra.getDistance_cm();
  if(voiture > 15.0 && voiture < 100.0 && feu.getMode()== NUIT){
    Serial.println("VOITURE");
  }
}
