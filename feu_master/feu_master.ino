/* include libraries */
#include <luminosite.h>
#include <tempo.h>
#include <feu.h>

/* pin define */
 /** Bluetooth **/
  const byte KeyS = 6; // Serial2
  const byte KeyP = 7; // Serial3 

  /** Feux Tricolore **/
  const int LV = 49;
  const int LO = 51;
  const int LR = 53;
  
  const byte pietonPin = 8; // simulation du bouton pieton

  /** Luminosite **/
  const int LightPin = A0; 

/* Constante define */
    
  Feu feu(LV, LO, LR);
  Luminosite lum(LightPin);

  // compte le nombre de ms ecoulees depuis le lancement du programme
  //int C = 0;
  int t = 0;

  
void setup() {
  
  lum.init();
  feu.init();

  pinMode(pietonPin, INPUT);
  pinMode(KeyS, OUTPUT);
  pinMode(KeyP, OUTPUT);
  digitalWrite(KeyS, LOW);
  digitalWrite(KeyP, LOW);
  
  Serial.begin(9600);  // Monitor
  Serial2.begin(9600); // Secondaire
  Serial3.begin(9600); // Principal

  delay(1000); // Délai pour laisser les HC-05 de capter KEY = 'LOW'
  digitalWrite(KeyS, HIGH);
  digitalWrite(KeyP, HIGH);
  delay(100);
  
  //Paramétrage du bluetooth “axe Sceondaire” 
  //(mode master, pwd=1234, name=master-s)
  Serial.println("parametrage du BT Maser axe Secondaire");
  printMsgToSerial2("AT+ROLE=1");       // configure role master
  printMsgToSerial2("AT+PSWD=1234");    // connect using password 1234
  printMsgToSerial2("AT+NAME=HC05-Master-s");
  printMsgToSerial2("AT+RMAAD");        // vide le cache des connexions précédentes 
  printMsgToSerial2("AT+CMODE=1");      // accepte toute connexion
  delay(200);
  printMsgToSerial2("AT+INIT");         // initialise l'étape de connexion
  printMsgToSerial2("AT+LINK=98d3,11,fc12bb"); // se connecte à l'adresse indiqué
  

  //Le maître paramètre le second bluetooth “axe Principal” 
  //(mode master, pwd=1234, name=master-p)
  Serial.println("parametrage du BT Master axe Principal");
  printMsgToSerial3("AT+ROLE=1");
  printMsgToSerial3("AT+PSWD=1234");
  printMsgToSerial3("AT+NAME=HC05-Master-p");
  printMsgToSerial3("AT+RMAAD");
  printMsgToSerial3("AT+CMODE=1");
  
  delay(200);
  printMsgToSerial3("AT+INIT");
  printMsgToSerial3("AT+LINK=98d3,c1,df37a8");

  Serial.println("========> FIN de la configuration");
  digitalWrite(KeyS, LOW);
  digitalWrite(KeyP, LOW);
  delay(100);
  feu.setMode(NORMAL);
}
int count=0;
void loop() {
  
  // Lecture de la luminosité ambiante
  float luminosite = lum.getLuminosite(); // dans [0, 1]
  
  // Lecture du bouton des piétons du feu maître
  int pieton = digitalRead(pietonPin);

  // recupere les messages sur la liaison bluetooth
  String msg_s = "";
  String msg_p = "";
  if(Serial2.available()) msg_s = Serial2.readString();
  if(Serial3.available()) msg_p = Serial3.readString();
  
  if(msg_s != "") Serial.println("[Secondaire]: "+ msg_s);
  if(msg_p != "") Serial.println("[Principal]: "+ msg_p);
  
  // mode actualisé uniquement quand le feu maître est VERT
  if(luminosite < LUMINOSITE_JOUR && t==0){
    // paramètre le feu en mode NUIT
    Serial.println("Mise en mode NUIT");
    feu.setMode(NUIT);
    
    // transmission du mode aux esclaves
    Serial2.println("MODE:1");
    Serial3.println("MODE:1");
  }else if(luminosite > LUMINOSITE_JOUR && t==0){
    // paramètre le feu en mode NORMAL
    Serial.println("Mise en mode JOUR");
    feu.setMode(NORMAL);
    // transmission du mode aux esclaves
    Serial2.println("MODE:0");
    Serial3.println("MODE:0");
  } 


  if(pieton==HIGH && feu.getCouleur()==VERT){
    // pieton a appuyé sur le bouton, on passe au temps du orange
    t = TV;
    feu.setMode(NORMAL);
  }
  else if(msg_p.startsWith("PIETON") && feu.getCouleur()==VERT){
    // pieton au feu esclave principal appuyé sur le bouton, on passe au temps du orange
    t = TV;
    feu.setMode(NORMAL);
  }
  else if(msg_s.startsWith("PIETON") && feu.getMode()==NORMAL && feu.getCouleur()==ROUGE){
    // pieton au feu esclave secondaire appuyé sur le bouton, on passe au temps du orange
    t = 2*TV+TO;
    feu.setMode(NORMAL);
  }

  // Le maître reçoie qu'il y a une voiture en attente.
  if(msg_s.startsWith("VOITURE") && feu.getMode()==NUIT){
    // le feu esclave maître passe à l'orange
    t = TV;
    // le feu repasse en NORMAL pour faire écouler le cycle
    // qui change la couleur des feux
    feu.setMode(NORMAL);
  }
  
  if(t%1000 == 0) Serial.println(String(t));
  // gestion de la couleur, selon le mode
  if(feu.getMode() == NORMAL){
    // Traitement du mode NORMAL
    if(t == 0){
      // maître VERT
      feu.setCouleur(VERT);
      Serial3.println("COLOR:2");
     
      // secondaire ROUGE
      Serial2.println("COLOR:0");
    }else if(t == TV){
      // maître ORANGE
      feu.setCouleur(ORANGE);
      Serial3.println("COLOR:1");
      
      // scondaire ROUGE
      Serial2.println("COLOR:0");
    }else if(t == TV + TO){
      // maître ROUGE
      feu.setCouleur(ROUGE);
      Serial3.println("COLOR:0");
      
      // secondaire VERT
      Serial2.println("COLOR:2");
    }else if(t == 2*TV + TO){
      // maître toujours ROUGE
      
      // secondaire ORANGE
      Serial2.println("COLOR:1");
    }else if(t == 2*(TV + TO) - dt){ // j'aime bien ca
      Serial.println("je suis un poisson lune!");
      
      // slave principal
      Serial3.println("COLOR:2");

      // slave secondaire
      Serial2.println("COLOR:0");
    }
  }else if(feu.getMode() == NUIT){
    // Traitement du mode NUIT
    if(t % 4000 == 0){ // evite de spammer la liaison série ptdr
      // feu VERT
      feu.setCouleur(VERT);
      Serial3.println("COLOR:2");
      
      // secondaire ROUGE
      Serial2.println("COLOR:0");
    }
  }else if(feu.getMode() == WARNING){
    // Traitement du mode WARNING
    if(t%(2*TW)==0){
      feu.setCouleur(ORANGE);
    }else if(t%(2*TW)==TW){
      feu.setCouleur(AUCUNE);
    }    
  }
  // change la couleur du feu et sa luminosité
  feu.updateLuminosite(0.0);
  delay(dt);
  t += dt;
  t %= TTOTAL;
  //C += 1;
  
}

// aka Secondaire HC-05
void printMsgToSerial2(String msg) {
  Serial2.println(msg);
  Serial.println(msg);
  while(!Serial2.available()) {}
  Serial.println(Serial2.readString());
}

// aka Principal HC-05
void printMsgToSerial3(String msg) {
  Serial3.println(msg);
  Serial.println(msg);
  while(!Serial3.available()) {}
  Serial.println(Serial3.readString());
}
